# Installation

1. [Install Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) and [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
2. Start Minikube

  ````bash
  minikube start
  ````

3. Start the services:

  ````bash
  kubectl apply -f ./
  ````

4. Log in to Keycloak and set up dcm4chee-arc-ui Clients

  * Go to http://192.168.99.100:30088/auth/admin/dcm4che/console
  * Log in with user: admin, password: admin


5. Log in to Keycloak and set up the ohif-viewer Client
  * When setting up ohif-viewer Client, set it to 'confidential' and Save.
  * When setting up the ohif-viewer Client, copy the client secret into the configuration file in ohif.yml
  * Copy Realm > Keys > Public Key into the configuration file in ohif.yml


6. Replace the OHIF service with the updated configuration: ````kubectl replace --force -f ./ohif.yml````

7. Test the installation:
  * dcm4chee UI: http://192.168.99.100:30088/dcm4chee-arc/ui2
  * OHIF Viewer UI: http://192.168.99.100:30088/
  * Keycloak Admin UI: http://192.168.99.100:30088/auth/admin/dcm4che/console/
  * Minikube dashboard: http://192.168.99.100:30000/


### Notes:
- OAuth tokens do not currently refresh. You can use Keycloak to set the token lifespan here: http://192.168.99.100:30088/auth/admin/dcm4che/console/#/realms/dcm4che/token-settings
